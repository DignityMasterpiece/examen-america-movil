import { createStore } from 'redux';

const reducer = (state, action) => {
    if(action.type === 'CHANGE_TO_LANGUAGE' ){
        return {
            ...state,
            language:action.language
        }        
    }
    return state;
};

export default createStore(reducer,{ language: null })