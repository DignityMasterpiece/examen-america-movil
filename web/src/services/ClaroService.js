const getAllInformation = () => `https://mfwkweb-api.clarovideo.net//services/content/list?api_version=v5.86&authpn=webclient&authpt=tfg1h3j4k6fd7&format=json&region=mexico&device_id=web&device_category=web&device_model=web&device_type=web&device_so=chrome&device_manufacturer=generic&HKS=m9kpsvofksdglsl494resh4h16&quantity=40&order_way=DESC&order_id=200&level_id=GPS&from=0&node_id=43864`;
const getMovieInformation = id => `https://mfwkweb-api.clarovideo.net/services/content/data?device_id=web&device_category=web&device_model=web&device_type=web&format=json&device_manufacturer=generic&authpn=webclient&authpt=tfg1h3j4k6fd7&api_version=v5.86&region=mexico&HKS=m9kpsvofksdglsl494resh4h16&group_id=${id}`;
var xhr = new XMLHttpRequest();

const ClaroService = {

    getAll: () => {
        return new Promise ((resolve,reject)=>{
            if(xhr) {    
                xhr.open('GET', getAllInformation(), true);
                xhr.onreadystatechange =   function () {
                    if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                        resolve(JSON.parse(xhr.responseText));
                    }
                };
                xhr.send(); 
              }
        })
    },
    getMovieInformation: (id) => {
        return new Promise ((resolve,reject)=>{
            if(xhr) {    
                xhr.open('GET', getMovieInformation(id), true);
                xhr.onreadystatechange =   function () {
                    if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                        resolve(JSON.parse(xhr.responseText));
                    }
                };
                xhr.send(); 
              }
        })
    },
}
export default ClaroService;