import React, { Component } from "react";
import './login.scss';
import swal from 'sweetalert';
import UserService from '../../services/UserService'
import { withRouter } from "react-router-dom";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      credentials: {
        email: 'notorious@gmail.com',
        password: 'ppp'
      }
    }
  }

  handleOnChange = (value, prop) => {
    value = value.trim()
    this.setState({
      credentials: {
        ...this.state.credentials,
        [prop]: value
      }
    })
  }

  submit = async (event) => {
    event.preventDefault();
    UserService.login(this.state.credentials).then(user => {
      console.log(user);
      
      localStorage.setItem('user', JSON.stringify(user));
      swal({
        title: 'bienvenido!',
        text: user.email,
        icon: 'success'
      })
      this.props.history.push('/')
    }).catch(e => {
      console.log(e.message);
      swal({
        title: 'error!',
        text: e.message,
        icon: 'error'
      })
    })
  }

  render() {
    const { email, password } = this.state.credentials
    return (
      <div id='login-container' >
        <div className="section"></div>
        <main>
          <center>
            <img
              className="responsive-img"
              //src="/wall_default2.jpg" 
              alt=''
            />
            <div className="section"></div>
            {
              //<h5 className="indigo-text">Please, login into your account</h5>
            }
            <div className="section"></div>
            <div className="container">
              <div
                className="z-depth-1 form-container-login lighten-4 row"
                style={{ display: 'inline-block', padding: '32px 48px 0px 48px', border: '1px solid #EEE' }}
              >
                <form className="col s12" >
                  <div className='row'>
                    <div className='col s12'>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='input-field col s12'>
                      <input
                        className=''
                        type='email'
                        name='email'
                        id='email'
                        value={email || ''}
                        onChange={e => this.handleOnChange(e.target.value, 'email')}
                      />
                      <label htmlFor='email'>Enter your email</label>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='input-field col s12'>
                      <input
                        className='validate'
                        type='password'
                        name='password'
                        id='password'
                        value={password || ''}
                        onChange={e => this.handleOnChange(e.target.value, 'password')}
                      />
                      <label htmlFor='password'>Enter your password</label>
                    </div>
                    <label
                      style={{ float: 'right' }}
                    >
                      <a className='white-text' href='#!'><b>Forgot Password?</b></a>
                    </label>
                  </div>

                  <br />
                  <center>
                    <div className='row'>
                      <button
                        name='btn_login'
                        className='col s12 btn btn-large waves-effect'
                        onClick={e => this.submit(e)}
                      >
                        Login
                      </button>
                    </div>
                  </center>
                </form>
              </div>
            </div>
            <a href="#!" onClick={
              e => {
                this.props.history.push('/signup')
              }
            }>Create account</a>
          </center>
          <div className="section"></div>
          <div className="section"></div>
        </main>
      </div>
    );
  }
}
export default withRouter(Login);
