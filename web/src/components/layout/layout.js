import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "materialize-css/dist/css/materialize.min.css";
import M from "materialize-css";
import "./layout.scss";
import Header from "../header/header";
import Home from "../home/home";
import Movie from "../home/movie";
import Footer from "../footer/footer";

class Layout extends Component {
  componentDidMount() {
    M.AutoInit();
  }
  render() {
    return (
      [
        <Header key={'header'}>
        </Header>,
        <main key={'main'} className='main-container-layout' >
          <div className="container">
            <BrowserRouter>
              <Switch>
                <Route exact path={this.props.match.path} component={Home} />
                <Route
                  exact
                  path={this.props.match.path + "fall"}
                  component={() => {
                    return <b>Fall a part</b>;
                  }}
                />
                <Route path={this.props.match.path+":id"} component={Movie}/>
                <Route
                  path={this.props.match.path + "**"}
                  component={() => {
                    let { pathname } = this.props.location;
                    pathname = pathname.substr(1);
                    return <b>NOT FOUND {pathname}</b>;
                  }}
                />
              </Switch>
            </BrowserRouter>
          </div>
        </main>,
        <Footer key={'footer'} >
        </Footer>
      ]
    );
  }
}
export default Layout;
