import React, { Component } from "react";
import { injectIntl } from 'react-intl';
import M from "materialize-css";
import ClaroService from '../../services/ClaroService'
import './home.scss'

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      all: [],
      groups: []
    }
  }
  async componentDidMount() {
    const all = await ClaroService.getAll();
    let { groups } = all.response;
    this.setState({ all: groups, groups }, () => {
      M.AutoInit();
      document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.tooltipped');
        console.log(elems, 'element')
        M.Tooltip.init(elems, {});
      });
    })

  }

  contentRender = () => {
    let { groups } = this.state;
    if (!groups.length > 0)
      return (<h3>Sin coincidencias.</h3>)
    return groups.map(media => {
      return (
        <div className="column-4" key={media.id}>
          <img src={media.image_background} alt={media.title}
            className="tooltipped"
            data-position="left"
            data-delay="50"
            data-tooltip={media.title}
            data-background-color="red lighten-3"
            onClick={async () => {
              const tools = document.getElementsByClassName("material-tooltip");
              while (tools.length > 0) tools[0].remove();
              const movie = await ClaroService.getMovieInformation(media.id);
              this.props.history.push(`/${media.id}`, { movie });
            }}
          />
        </div>
      )
    })
  }
  search = (text) => {
    const regex = new RegExp(text, 'i');
    const { all } = this.state;
    this.setState({ groups: all.filter(movie => regex.exec(movie.title) !== null) })
  }

  render() {
    // const { formatMessage } = this.props.intl;
    const { all } = this.state;
    if (!all.length > 0)
      return (<h3>Sin Informacion.</h3>)
    return (
      <div id='home-catalog'>
        <div className="row">
          <div className="col s12">
            <div className="row">
              <div className="input-field col s12">
                <i className="material-icons prefix">movie</i>
                <input
                  type="text"
                  id="autocomplete-input"
                  className="autocomplete"
                  onKeyUp={e => this.search(e.target.value)} />
                <label hr="autocomplete-input">Filtre por titulo</label>
              </div>
            </div>
          </div>
        </div>
        <div className='catalog row'>
          {this.contentRender()}
        </div>
      </div>
    );
  }
}
export default injectIntl(Home);
