
import React from 'react';
import './home.scss';
import { withRouter } from 'react-router-dom';

const Movie = (props) => {
    if (!props.location.state) {
        return 'algo salio mal'
    } else {
        const { common } = props.location.state.movie.response.group
        return (
            <div id='movie-container' className="row">
                <div className="col s12 m7">
                    <div className="card">
                        <div className="card-image">
                            <img src={common.image_background} alt='' />
                            <span className="card-title">{common.title}</span>
                        </div>
                        <div className="card-content">
                            <p>{common.large_description}</p>
                        </div>
                        <div className="card-action">
                            <a href="" onClick={() => { alert('lol') }} >Reproducir</a>
                            <a href="" onClick={() => props.history.push('/')}> atras </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Movie);