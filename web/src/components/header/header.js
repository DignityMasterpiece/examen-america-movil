import React, { Component } from "react";
import './header.scss'
import $ from 'jquery';
import { withRouter } from "react-router-dom";
import UserService from '../../services/UserService'
import  InternationalizationSelect from '../internationalizationSelect/internationalizationSelect'
import swal from 'sweetalert';
import { injectIntl } from 'react-intl';


class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  logOut = async (e) => {
    console.log(e.target);
    const userResponse = await swal({
      title: "estas seguro de cerrar sesion?😀",
      text: "esta accion te desconectara del sistema?!",
      icon: "warning",
      buttons: true,
    })
    if (userResponse) {
      localStorage.removeItem('user');
      this.props.history.push('/login')

      return;
      UserService.logOut().then(result => {
        // localStorage.removeItem('user');
        if ($('.sidenav-overlay'))
          $('.sidenav-overlay').remove()
        this.props.history.push('/login')
      }).catch(e => {
        swal({
          title: "error",
          text: "Algo salio mal",
          icon: "error",
        })
      })
    } else {
      swal("Estas aun dentro!");
    }
  }
  render() {
    const { formatMessage } = this.props.intl;

    return (
      <header className='header-layout'>
        <nav role="navigation" className="nav-extended">
          <div className="nav-wrapper">
            <a data-target="slide-out" href='#vav' className="sidenav-trigger">
              <i className="material-icons">menu</i>
            </a>
            <ul id="nav-mobile" className="right hide-on-med-and-down">
              {
                /**
                <li>
                  <a href="sass.html">Sass</a>
                </li>
                <li>
                  <a href="badges.html">Components</a>
                </li>
                 */
              }
              <li>
                  <InternationalizationSelect/>
              </li>
            </ul>
          </div>
        </nav>
        <ul id="slide-out" className="sidenav">
          <li>
            <a
              className=' text-view logout'
              href='#logout'
              onClick={e => this.logOut(e)}>
              <span className='material-icons'>exit_to_app</span>
            </a>
            <div className="user-view">
              <div className="background">
                <img src="/wall_default2.jpg" alt="" />
              </div>
              <a href="#user">
                <img className="circle" src="/default.jpg" alt="" />
              </a>
              <a href="#name">
                <span className="text-view name">John Doe</span>
              </a>
              <a href="#email">
                <span className="text-view email">jdandturk@gmail.com</span>
              </a>
            </div>
          </li>
          <li>
            <a href="#!">
              <i className="material-icons">cloud</i>First Link With Icon
            </a>
          </li>
          <li>
            {formatMessage({ id: 'signup.last_name' })}
          </li>
          <li>
            <div className="divider" />
          </li>
          <li>
            <a href='#sub' className="subheader">Subheader</a>
          </li>
          <li>
            <a className="waves-effect" href="#!">
              Third Link With Waves
            </a>
          </li>
        </ul>
      </header>
    );
  }
}
export default withRouter(injectIntl(Header));
